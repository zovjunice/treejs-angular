'use strict';

/**
 * @ngdoc overview
 * @name jstreeApp
 * @description
 * # jstreeApp
 *
 * Main module of the application.
 */
angular
  .module('jstreeApp', ['jsTree.directive']);
