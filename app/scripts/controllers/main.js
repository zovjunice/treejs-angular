'use strict';

/**
 * @ngdoc function
 * @name jstreeApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the jstreeApp
 */
angular.module('jstreeApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
